package com.guigarage.incubator.emoji;

import com.guigarage.controls.EmojiUtil;
import com.guigarage.controls.MaximizePane;
import com.guigarage.ui.IconFonts;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;

public class EmojiTextField extends Application {

    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        TextField textField = new TextField();
        textField.getStyleClass().add("emoji");
        textField.setOnKeyPressed(e -> {
            if (e.getCode().equals(KeyCode.DOWN)) {
                EmojiUtil.showEmojiPopup(textField, true, ev -> textField.appendText(ev.getEmoji().toString()));
            }
        });
        MaximizePane pane = new MaximizePane();
        pane.setPadding(new Insets(24));
        pane.getChildren().add(textField);
        pane.setMaxWidth(800);
        Scene scene = new Scene(pane);
        IconFonts.addToScene(scene);
        scene.getStylesheets().add(EmojiTextField.class.getResource("skin.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void stop() throws Exception {
        System.exit(0);
    }
}
