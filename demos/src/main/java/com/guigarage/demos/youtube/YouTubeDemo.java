package com.guigarage.demos.youtube;

import com.guigarage.youtube.YouTubePlayer;
import com.guigarage.youtube.YouTubeVideo;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class YouTubeDemo extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        YouTubeVideo youTubeVideo = new YouTubeVideo("W-LCvTa5MQQ");
        primaryStage.setScene(new Scene(new YouTubePlayer(youTubeVideo)));
        primaryStage.setOnCloseRequest(e -> System.exit(0));
        primaryStage.show();
    }

    public static void main(String... args) {
        launch(args);
    }
}
